# Coralogix JS

This is a front-end api for the Coralogix logger.

Usage: 

1. Include the JS in your head section
```html
<script src="/path/to/coralogix.min.js"></script>
```

2. Initialize the Coralogix logger

```html
<script>
      (function() {
        coralogix.init({
          debug: false,
          privateKey: 'Your key goes here',
          applicationName: 'Your application name',
          subsystemName: 'Your subsystem',
          computerName: 'Your computer name',
          queue: {
            max_queue: 5, // Maximum messages to set in queue
            max_time: 10 // Maximum time (in seconds) to wait before pushing the queue
          }
        });
      })()
</script>
```

3. Add your first call

```javascript
coralogix.debug(
	"Your log text", 
    "Your category", 
    "Your class name", 
    "Your method name", 
    "Your thread id"); 
```

**Note:** if any information is not required, send an empty string.

## Debugging
If you require debugging, you can set the **debug** var to **true** under the initialization.