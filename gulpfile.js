var gulp = require('gulp');
var jsmin = require('gulp-jsmin');
var rename = require('gulp-rename');
var concat = require('gulp-concat');

gulp.task('scripts', function() {
  gulp.src([
    'src/js/main.js',
    'src/js/queue.js'
  ])
    .pipe(concat('coralogix.js'))
    .pipe(gulp.dest('dist/js'))
    .pipe(rename({suffix: '.min'}))
    .pipe(jsmin())
    .pipe(gulp.dest('dist/js'));
})

gulp.task('default', function() {

  gulp.watch('src/js/**', function(event) {
    gulp.run('scripts');
  })

})
