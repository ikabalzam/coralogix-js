(function() {
  var queue = {
    updating: false,

    configuration: {
      max_queue: 5, // items
      max_time: 5, // seconds
    },

    items: [],

    getItemCount: function() {
      return this.items.length;
    },

    log: function(severity, item) {
      // TODO: Add validation here
      coralogix.debuglog('logging item: ' + severity);
      this.items.push({
        timestamp: Date.now(),
        severity: severity,
        text: (item.text) ? item.text : 'no data',
        category: (item.category) ? item.category : 'no category',
        className: (item.className) ? item.className : 'no class name',
        methodName: (item.methodName) ? item.methodName : 'no method name',
        threadId: (item.threadId) ? item.threadId : 'no thread id',
      });
      this.shouldUpdate();
    },

    shouldUpdate: function() {
      if (this.getItemCount() >= this.configuration.max_queue) {
        coralogix.debuglog('should update is true, updating');
        this.update();
      } else {
        coralogix.debuglog('not updating yet, has only: ' + this.getItemCount());
      }
    },

    startUpdater: function() {
      if (coralogix.configuration.queue && coralogix.configuration.queue.max_time) {
        coralogix.updateTimer = window.setInterval(function() { coralogix.queue.update() }, coralogix.configuration.queue.max_time * 1000);
      } else {
        console.log('[coralogix] could not start the auto updater');
      }
    },

    init: function (configuration) {
      Object.assign(this.configuration, configuration);
      coralogix.configuration.queue = this.configuration;
      this.startUpdater();
    },

    update: function() {
      if (this.updating) { return false; }
      if (!this.items.length) { return false; }

      let tmpitems = this.items.slice();
      this.updating = true;
      this.items = []; // clear the queue;
      coralogix.update(tmpitems);
      this.updating = false;
    }
  }

  if (coralogix) { coralogix.queue = queue; }
})();
