var coralogix = {
  version: '0.0.2',

  updateTimer: null,

  configuration: {
    privateKey: '',
    debug: false,
    queue: {}
  },

  severities: {
    debug: 1,  verbose: 2,  info: 3,
    warn: 4,  error: 5, critical: 6
  },

  url: 'https://api.coralogix.com/api/v1/logs',

  update: function(items) {
    let xhr = new XMLHttpRequest();

    let data = {
      privateKey: this.configuration.privateKey,
      applicationName: this.configuration.applicationName,
      subsystemName: this.configuration.subsystemName,
      computerName: this.configuration.computerName,
      logEntries: items
    };

    xhr.open('POST', this.url);
    xhr.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
    xhr.onload = function() {
      if (xhr.status !== 200) {
        console.log('[coralogix] logger failed: ' + xhr.responseText + ' with error code: ' + xhr.status);
      }
    };
    xhr.send(JSON.stringify(data));
  },

  init: function (configuration) {
    Object.assign(this.configuration, configuration);
    this.queue.init(configuration.queue); // init the queue
  },

  debuglog: function(log) {
    if (this.configuration.debug) { console.log("[coralogix] " + log); }
  },

  // Logger
  /**
   * @param item
   * @param text
   * @param category
   * @param className
   * @param methodName
   * @param threadId
   */
  debug:    function (text, category, className, methodName, threadId) { this.queue.log(this.severities.debug,     { text: text, category: category, className: className, methodName: methodName, threadId: threadId}); },
  verbose:  function (text, category, className, methodName, threadId) { this.queue.log(this.severities.verbose,   { text: text, category: category, className: className, methodName: methodName, threadId: threadId}); },
  info:     function (text, category, className, methodName, threadId) { this.queue.log(this.severities.info,      { text: text, category: category, className: className, methodName: methodName, threadId: threadId}); },
  warn:     function (text, category, className, methodName, threadId) { this.queue.log(this.severities.warn,      { text: text, category: category, className: className, methodName: methodName, threadId: threadId}); },
  error:    function (text, category, className, methodName, threadId) { this.queue.log(this.severities.error,     { text: text, category: category, className: className, methodName: methodName, threadId: threadId}); },
  critical: function (text, category, className, methodName, threadId) { this.queue.log(this.severities.critical,  { text: text, category: category, className: className, methodName: methodName, threadId: threadId}); }

};
(function() {
  var queue = {
    updating: false,

    configuration: {
      max_queue: 5, // items
      max_time: 5, // seconds
    },

    items: [],

    getItemCount: function() {
      return this.items.length;
    },

    log: function(severity, item) {
      // TODO: Add validation here
      coralogix.debuglog('logging item: ' + severity);
      this.items.push({
        timestamp: Date.now(),
        severity: severity,
        text: (item.text) ? item.text : 'no data',
        category: (item.category) ? item.category : 'no category',
        className: (item.className) ? item.className : 'no class name',
        methodName: (item.methodName) ? item.methodName : 'no method name',
        threadId: (item.threadId) ? item.threadId : 'no thread id',
      });
      this.shouldUpdate();
    },

    shouldUpdate: function() {
      if (this.getItemCount() >= this.configuration.max_queue) {
        coralogix.debuglog('should update is true, updating');
        this.update();
      } else {
        coralogix.debuglog('not updating yet, has only: ' + this.getItemCount());
      }
    },

    startUpdater: function() {
      if (coralogix.configuration.queue && coralogix.configuration.queue.max_time) {
        coralogix.updateTimer = window.setInterval(function() { coralogix.queue.update() }, coralogix.configuration.queue.max_time * 1000);
      } else {
        console.log('[coralogix] could not start the auto updater');
      }
    },

    init: function (configuration) {
      Object.assign(this.configuration, configuration);
      coralogix.configuration.queue = this.configuration;
      this.startUpdater();
    },

    update: function() {
      if (this.updating) { return false; }
      if (!this.items.length) { return false; }

      let tmpitems = this.items.slice();
      this.updating = true;
      this.items = []; // clear the queue;
      coralogix.update(tmpitems);
      this.updating = false;
    }
  }

  if (coralogix) { coralogix.queue = queue; }
})();
